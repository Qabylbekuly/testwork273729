<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class APIkey
{
    public function handle($request, Closure $next)

    {

        if ($request->apikey == '') {
            return response("Invalid access key");
        } else {
            if ($request->apikey != env('API_KEY')) {
                return response("Invalid access key");
            } else {
                return $next($request);
            }
        }
    }
}
