<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Http\Resources\CategoryShowResource;
use App\Services\CategoryService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    private CategoryService $service;

    public function __construct(CategoryService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        return $this->service->getAsPaginated();
    }

    public function store(CategoryRequest $request)
    {
        return $this->service->store($request->validated());
    }

    public function show(string $id)
    {
        return new CategoryShowResource($this->service->getModelById($id));
    }


    public function update(CategoryRequest $request, string $id)
    {
        return $this->service->update($id, $request->validated());
    }

    public function destroy(string $id)
    {
        return $this->service->destroy($id);
    }
}
