<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\AttachRequest;
use App\Http\Requests\DetachRequest;
use App\Http\Requests\ProductRequest;
use App\Http\Resources\ProductShowResource;
use App\Services\ProductService;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    private ProductService $service;

    public function __construct(ProductService $service)
    {
        $this->service = $service;
    }
    public function index(Request $request)
    {
        return $this->service->search($request);
    }
    public function show(string $id)
    {
        return new ProductShowResource($this->service->getModelById($id));
    }

    public function store(ProductRequest $request)
    {
        return response()->json([
            'data'=>$this->service->store($request->validated())
        ]);
    }

    public function update(ProductRequest $request, string $id)
    {
        return $this->service->update($id, $request->validated());
    }

    public function attach(AttachRequest $request, string $id)
    {
        return $this->service->attach($id, $request->validated());
    }

    public function detach(DetachRequest $request, string $id)
    {
        return $this->service->detach($id, $request->validated());
    }

    public function destroy(string $id)
    {
        return $this->service->destroy($id);
    }

}
