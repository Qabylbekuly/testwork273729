<?php

namespace App\Services;

use App\Models\Category;
use App\Models\CategoryProduct;
use App\Repositories\CategoryProductRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\ProductRepository;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CategoryService
{
    private Category $category;
    private CategoryRepository $categoryRepository;
    private CategoryProductRepository $productCategoryRepository;

    public function __construct(
        CategoryRepository        $categoryRepository,
        CategoryProductRepository $productCategoryRepository,
    )
    {
        $this->categoryRepository = $categoryRepository;
        $this->productCategoryRepository = $productCategoryRepository;
    }

    public function getAsPaginated()
    {
        return $this->categoryRepository->paginated();
    }

    public function store(array $data)
    {
        DB::beginTransaction();
        try {
            $this->category = $this->categoryRepository->create($data);
            DB::commit();
            return $this->category;
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function getModelById(string $id)
    {
        $model = $this->categoryRepository->find($id);
        if (!$model) throw new NotFoundHttpException();
        return $model;
    }

    public function update(string $id, array $data)
    {
        DB::beginTransaction();

        try {
            $this->category = $this->categoryRepository->find($id);
            if (!$this->category) throw new NotFoundHttpException();
            $data['id'] = $this->category->id;
            $this->category = $this->categoryRepository->update($id, $data);
            DB::commit();
            return $this->category;
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function destroy(string $id)
    {
        $category = $this->categoryRepository->find($id);
        if (!$category) throw new NotFoundHttpException();
        $this->categoryRepository->destroy($id);
        return [];
    }
}
