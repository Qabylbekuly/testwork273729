<?php

namespace App\Services;

use App\Models\Logistics\Delivery;
use App\Models\Product;
use App\Models\CategoryProduct;
use App\Repositories\CategoryProductRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\ProductRepository;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProductService
{
    protected Product $model;
    private ProductRepository $productRepository;
    private CategoryRepository $categoryRepository;
    private CategoryProductRepository $productCategoryRepository;


    public function __construct(
        ProductRepository         $productRepository,
        CategoryRepository        $categoryRepository,
        CategoryProductRepository $productCategoryRepository,
    )
    {
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
        $this->productCategoryRepository = $productCategoryRepository;
    }


    public function search($request)
    {
        $products = Product::query();
        if ($request->q) {
            $products->where('product_name', $request->q);
            $products->orwhereHas('categories', function ($query) use ($request) {
                $query->where('category_name', 'like', '%' . $request->q . '%');
            })->with(['categories' => function ($query) use ($request) {
                $query->where('category_name', 'like', '%' . $request->q . '%');
            }])->get();
        }
        if (isset($request->sortby)) {
            $sortbyType = isset($request->sort_type) ? $request->sort_type : 'ASC';
            $products->orderby($request->sortby, $sortbyType);
        }
        if (isset($request->price)) {
            $products->whereBetween('product_price', $request->price);
        }
        return $products->with('categories')->paginate(10);

    }


    public
    function getAsPaginated()
    {
        return $this->productRepository->with('categories')->paginate();
    }

    public
    function getModelById(string $id)
    {
        $model = $this->productRepository->find($id);
        if (!$model) throw new NotFoundHttpException();
        return $model;
    }

    public
    function store(array $data)
    {
        DB::beginTransaction();
        try {

            $this->model = $this->productRepository->create($data);
            $this->model->categories()->attach($data['product_categories']);
            DB::commit();
            $this->model->categories = $this->model->categories;
            return $this->model;
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public
    function update(string $id, array $data)
    {
        DB::beginTransaction();

        try {
            $this->model = $this->productRepository->find($id);
            if (!$this->model) throw new NotFoundHttpException();
            $data['id'] = $this->model->id;
            $this->model = $this->productRepository->update($id, $data);
            DB::commit();
            return $this->model;
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public
    function attach(string $id, array $data)
    {
        DB::beginTransaction();

        try {
            $this->model = $this->productRepository->find($id);
            if (!$this->model) throw new NotFoundHttpException();
            $data['id'] = $this->model->id;
            $this->model->categories()->attach($data['category_id']);
            DB::commit();
            return $this->model;
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }


    public
    function destroy(string $id)
    {
        $product = $this->productRepository->find($id);
        if (!$product) throw new NotFoundHttpException();
        $this->productRepository->destroy($id);
        return [];
    }


}
