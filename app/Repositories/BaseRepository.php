<?php


namespace App\Repositories;

use Illuminate\Database\Connection;
use Illuminate\Database\Eloquent\Model;

class BaseRepository
{
    public $sortBy = 'created_at';
    public $sortOrder = 'asc';

    /**
     * @var Model
     */
    protected $model;

    /**
     * Возвращает модель
     *
     * @return Model
     */
    public function getModel()
    {
        return $this->model;
    }

    public function getTable()
    {
        return $this->model->getTable();
    }

    /**
     * Возвращает подключение к БД
     *
     * @return Connection
     */
    public function getConnection()
    {
        return $this->model->getConnection();
    }

    /**
     * Get all elements on Model
     * @return mixed
     */
    public function all()
    {
        return $this->model
            ->orderBy($this->sortBy, $this->sortOrder)
            ->get();
    }

    /**
     * Get paginated elements on Model
     * @param $paginate
     * @return mixed
     */
    public function paginated($paginate = 30)
    {
        return $this
            ->model
            ->orderBy($this->sortBy, $this->sortOrder)
            ->paginate($paginate);
    }

    /**
     * Возвращает столбы
     *
     * @return array
     */
    public function getColumns()
    {
        return  $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getModel()->getTable());
    }

    public function prepareData(array $data)
    {
        $columns = $this->getColumns();
        $columns = array_flip($columns);

        return array_intersect_key($data, $columns);
    }

    /**
     * Insert any element to Database
     * @param $input
     * @return mixed
     */
    public function create($input)
    {
        $model = $this->model;
        $data = $this->prepareData($input);
        return $model->create($data);
    }

    /**
     * Find one element by id on Model
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return $this->getModel()->find($id);
    }

    /**
     * Delete one element by id on Model
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        return $this->getModel()->where('id',$id)->delete();
    }

    /**
     * Update one element by id on Model
     * @param $id
     * @param array $input
     * @return mixed
     */
    public function update($id, array $input)
    {
        $model = $this->find($id);
        $model->fill($input);
        $model->save();
        return $model;
    }

    /**
     * Get array with id and name ['id' => 'name']
     * @return mixed
     */
    public function getArray()
    {
        return $this->model->orderBy('name', 'asc')->pluck('name', 'id')->toArray();
    }
    public function with($relations)
    {
        return $this->model->with($relations);
    }
}
