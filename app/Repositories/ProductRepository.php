<?php


namespace App\Repositories;
use App\Models\Product;

class ProductRepository extends BaseRepository
{
    protected $model;
    public function __construct(Product $model)
    {
        $this->model = $model;
    }
    public function search($q)
    {
        return $this->model->where('name', 'Like', '%' . $q . '%')->paginate(12);
    }
}
