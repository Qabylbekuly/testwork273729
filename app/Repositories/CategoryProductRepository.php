<?php


namespace App\Repositories;

use App\Models\CategoryProduct;

class CategoryProductRepository extends BaseRepository
{
    protected $model;
    public function __construct(CategoryProduct $model)
    {
        $this->model = $model;
    }
    public function search($q)
    {
        return $this->model->where('name', 'Like', '%' . $q . '%')->paginate(12);
    }
}
