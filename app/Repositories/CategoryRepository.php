<?php


namespace App\Repositories;
use App\Models\Category;

class CategoryRepository extends BaseRepository
{
    protected $model;
    public function __construct(Category $model)
    {
        $this->model = $model;
    }
    public function search($q)
    {
        return $this->model->where('name', 'Like', '%' . $q . '%')->paginate(12);
    }
}
