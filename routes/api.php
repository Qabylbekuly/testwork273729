<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'App\Http\Controllers\API', 'middleware' => ['APIkey']], function () {
    Route::group(['prefix' => 'products'], function () {
        Route::get('', 'ProductController@index');
        Route::post('', 'ProductController@store');
        Route::get('{id}', 'ProductController@show');
        Route::post('{id}', 'ProductController@update');
        Route::delete('{id}', 'ProductController@destroy');
        Route::post('{id}/attach', 'ProductController@attach');
        Route::post('{id}/detach', 'ProductController@detach');
    });
    Route::group(['prefix' => 'categories'], function () {
        Route::get('', 'CategoryController@index');
        Route::post('', 'CategoryController@store');
        Route::get('{id}', 'CategoryController@show');
        Route::post('{id}', 'CategoryController@update');
        Route::delete('{id}', 'CategoryController@destroy');
    });
});
