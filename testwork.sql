-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Túın: 127.0.0.1:3306
-- Jasaý ýaqyty: 2022-04-14, 10:46
-- Server nusqasy: 8.0.24
-- PHP nusqasy: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Derekqor: `testwork`
--

-- --------------------------------------------------------

--
-- Keste úshin keste qurylymy `categories`
--

CREATE TABLE `categories` (
  `id` bigint UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_sort` int DEFAULT '500',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Keste úshin derekterdi damptaý `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `category_slug`, `category_sort`, `created_at`, `updated_at`) VALUES
(1, '23', '23', 123, '2022-04-14 00:09:27', '2022-04-14 00:10:42'),
(2, 'desktop', 'phones', 500, '2022-04-14 00:09:40', '2022-04-14 00:09:40'),
(4, 'Phones 3', 'phones 3', 500, '2022-04-14 00:38:55', '2022-04-14 00:38:55');

-- --------------------------------------------------------

--
-- Keste úshin keste qurylymy `category_product`
--

CREATE TABLE `category_product` (
  `id` bigint UNSIGNED NOT NULL,
  `category_id` bigint UNSIGNED NOT NULL,
  `product_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Keste úshin derekterdi damptaý `category_product`
--

INSERT INTO `category_product` (`id`, `category_id`, `product_id`, `created_at`, `updated_at`) VALUES
(5, 1, 3, NULL, NULL),
(6, 2, 3, NULL, NULL),
(12, 4, 3, NULL, NULL),
(13, 4, 3, NULL, NULL),
(14, 4, 3, NULL, NULL),
(15, 4, 3, NULL, NULL),
(16, 4, 3, NULL, NULL),
(17, 4, 3, NULL, NULL),
(18, 4, 3, NULL, NULL),
(19, 1, 4, NULL, NULL),
(20, 2, 4, NULL, NULL),
(21, 1, 5, NULL, NULL),
(22, 2, 5, NULL, NULL),
(23, 1, 6, NULL, NULL),
(24, 2, 6, NULL, NULL),
(25, 1, 7, NULL, NULL),
(26, 2, 7, NULL, NULL),
(27, 1, 8, NULL, NULL),
(28, 2, 8, NULL, NULL),
(29, 1, 9, NULL, NULL),
(30, 2, 9, NULL, NULL),
(31, 1, 10, NULL, NULL),
(32, 2, 10, NULL, NULL),
(33, 1, 11, NULL, NULL),
(34, 2, 11, NULL, NULL),
(35, 1, 12, NULL, NULL),
(36, 2, 12, NULL, NULL),
(37, 1, 13, NULL, NULL),
(38, 2, 13, NULL, NULL);

-- --------------------------------------------------------

--
-- Keste úshin keste qurylymy `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Keste úshin keste qurylymy `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Keste úshin derekterdi damptaý `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_04_08_125830_create_categories_table', 2),
(6, '2022_04_08_125915_create_products_table', 2),
(7, '2022_04_08_130031_create_product_categories_table', 2);

-- --------------------------------------------------------

--
-- Keste úshin keste qurylymy `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Keste úshin keste qurylymy `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Keste úshin keste qurylymy `products`
--

CREATE TABLE `products` (
  `id` bigint UNSIGNED NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_sort` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_price` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Keste úshin derekterdi damptaý `products`
--

INSERT INTO `products` (`id`, `product_name`, `product_description`, `product_slug`, `product_sort`, `product_price`, `created_at`, `updated_at`) VALUES
(3, 'qazaq', '1', '1', '1', 600, '2022-04-14 00:16:13', '2022-04-14 00:16:13'),
(4, '1', '1', '1', '1', 1, '2022-04-14 00:43:58', '2022-04-14 00:43:58'),
(5, '1', '1', '1', '1', 1, '2022-04-14 00:44:24', '2022-04-14 00:44:24'),
(6, '1', '1', '1', '1', 1, '2022-04-14 00:44:49', '2022-04-14 00:44:49'),
(7, '1', '1', '1', '1', 1, '2022-04-14 00:48:27', '2022-04-14 00:48:27'),
(8, '1', '1', '1', '1', 1, '2022-04-14 00:48:54', '2022-04-14 00:48:54'),
(9, '1', '1', '1', '1', 1, '2022-04-14 00:50:25', '2022-04-14 00:50:25'),
(10, '1', '1', '1', '1', 1, '2022-04-14 00:50:34', '2022-04-14 00:50:34'),
(11, '1', '1', '1', '1', 1, '2022-04-14 00:51:17', '2022-04-14 00:51:17'),
(12, '1', '1', '1', '1', 1, '2022-04-14 00:51:56', '2022-04-14 00:51:56'),
(13, '1', '1', '1', '1', 1, '2022-04-14 01:41:58', '2022-04-14 01:41:58');

-- --------------------------------------------------------

--
-- Keste úshin keste qurylymy `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Damptalǵan kesteler úshin ındekster
--

--
-- Keste úshin ındekster `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Keste úshin ındekster `category_product`
--
ALTER TABLE `category_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_product_category_id_foreign` (`category_id`),
  ADD KEY `category_product_product_id_foreign` (`product_id`);

--
-- Keste úshin ındekster `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Keste úshin ındekster `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Keste úshin ındekster `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Keste úshin ındekster `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Keste úshin ındekster `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Keste úshin ındekster `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Damptalǵan kesteler úshin AUTO_INCREMENT
--

--
-- Keste úshin AUTO_INCREMENT `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Keste úshin AUTO_INCREMENT `category_product`
--
ALTER TABLE `category_product`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- Keste úshin AUTO_INCREMENT `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Keste úshin AUTO_INCREMENT `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Keste úshin AUTO_INCREMENT `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Keste úshin AUTO_INCREMENT `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Keste úshin AUTO_INCREMENT `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Damptalǵan kesteler úshin shekteýler
--

--
-- Keste úshin shekteýler `category_product`
--
ALTER TABLE `category_product`
  ADD CONSTRAINT `category_product_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `category_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
